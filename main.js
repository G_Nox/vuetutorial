Vue.component('product', {
    template: `
    <div class="product">
        <div class="product-image"> 
            <img v-bind:src="selectedVariant.variantImage">    
        </div>
        <div class="product-info"> 
        <h1>{{title}}</h1>
        <p v-if="selectedVariant.amount > 10"> in Stock </p>
            <p v-else-if= "selectedVariant.amount<=10 && selectedVariant.amount > 0">nearly Sold Out</p>
            <p v-else>Out of Stock</p>
            <p v-show="Sale">On Sale!</p>
        <p>{{description}}</p>
        <ul>
            <li v-for="detail in details">{{detail}}</li>
        </ul>
        <div v-for="(variant, index) in variants" 
            :key=variant.variantId
            class="color-box"
            :style="{ backgroundColor: variant.variantColor || 'black'}">
            <p :style="{color:'white'}" @mouseover="updateProduct(index)" > 
            {{ variant.variantColor || variant.brand }} </p>
        </div>
            
        <button v-on:click="addToCart"
            :disabled="!selectedVariant.amount"
            :class="{ disabledButton: !selectedVariant.amount }">Add to Cart</button>
        <div class="cart">
            <p>Cart({{cart}})</p>
        </div>
        </div>
    </div>
    `,    
    data() {
        return {
        product: 'Socks',
        description: 'This is a Beautifull Sock! BYE IT NOW!',
        image: './Image/greenM.jpg',
        index: 0,
        details: ["80% cotton", "20% polyester", "Gender-neutral", "Vegan"],
        variants: [
            {
                variantID: 2234,
                variantColor: "green",
                variantImage: './Image/greenM.jpg',
                amount: 10,
                onSale:false
            },
            {
                variantID: 2235,
                variantColor: "blue",
                variantImage: './Image/blueM.jpg',
                amount: 50,
                onSale:true
            },
            {
                variantId: 2236,
                brand: "Puma",
                variantImage: './Image/puma.jpg',
                amount: 0,
                onSale:false
            },
            {
                variantID: 2237,
                brand: "Nike",
                variantImage: './Image/nike.jpg',
                amount: 3,
                onSale:false
            }
        ],
        cart:0
    }
},
methods: {  // methods = functions
    addToCart(){
        this.cart ++;
        console.log("Test");
    },
    updateProduct(index){
        this.index = index;
    }
},    
computed:{
    title() {
        return this.brand + ' ' + this.product;
    },
    selectedVariant(){
        return this.variants[this.index];
    },
    Sale(){
        return this.variants[this.index].onSale;
    }
}
})

var app = new Vue({
    el: '#app'
    
})
